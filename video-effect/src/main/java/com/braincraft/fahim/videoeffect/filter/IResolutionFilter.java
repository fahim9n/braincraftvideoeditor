package com.braincraft.fahim.videoeffect.filter;


import com.braincraft.fahim.videoeffect.Resolution;

/**
 * Created by sudamasayuki on 2018/01/07.
 */

public interface IResolutionFilter {
    void setResolution(Resolution resolution);
}

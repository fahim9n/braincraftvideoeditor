package com.braincraft.fahim.videoeditor.view;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.State;
import android.view.View;

import com.braincraft.fahim.videoeditor.utils.UIUtils;

public class VideoThumbSpacingItemDecoration extends RecyclerView.ItemDecoration {

    private int mSpace;
    private int mThumbnailsCount;

    public VideoThumbSpacingItemDecoration(int space, int thumbnailsCount) {
        mSpace = space;
        mThumbnailsCount = thumbnailsCount;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, State state) {
        int position = parent.getChildAdapterPosition(view);
        if (position == 0) {
            outRect.left = UIUtils.getScreenWidth()/2;
            outRect.right = 0;
        } else if (mThumbnailsCount > 10 && position == mThumbnailsCount - 1) {
            outRect.left = 0;
            outRect.right = UIUtils.getScreenWidth()/2;
        } else {
            outRect.left = 0;
            outRect.right = 0;
        }
    }
}

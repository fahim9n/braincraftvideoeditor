package com.braincraft.fahim.videoeditor.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

import com.braincraft.fahim.videoeditor.utils.SeekBarType;
import com.braincraft.fahim.videoeditor.utils.UIUtils;
import com.braincraft.fahim.videoeditor.R;

import java.text.DecimalFormat;


public class RangeSeekBar extends View {

    private static final String TAG = RangeSeekBar.class.getSimpleName();



    private SeekBarType seekBarType;
    private double absoluteMinValuePrim, absoluteMaxValuePrim;
    private double normalizedMinValue = 0d;
    private double normalizedMaxValue = 1d;
    private long min_cut_time = 3000;
    private double normalizedMinValueTime = 0d;
    private double normalizedMaxValueTime = 1d;
    private int mScaledTouchSlop;
    private Bitmap mBitmapBlack;
    private Bitmap mBitmapPro;
    private Paint paint;
    private Paint rectPaint;
    private Paint mGrayPaint;
    private Paint mShadowPaint;
    private int thumbWidth=0;
    private int thumbHeight=0;
    private float thumbHalfWidth;
    private final float padding = 0;
    private float thumbPaddingTop = 0;
    private boolean isTouchDown;
    private boolean notifyWhileDragging = false;
    private double thumbRatio;
    Bitmap leftSeekBarImage,rightSeekBarImage;
    private double exceptionalLentgh;

    public double getExceptionalLentgh() {
        return exceptionalLentgh;
    }

    public void setExceptionalLentgh(long exceptionalLentgh) {
        this.exceptionalLentgh = valueToNormalized(exceptionalLentgh);
    }

    public enum Thumb {
        MIN, MAX
    }

    public RangeSeekBar(Context context) {
        super(context);
    }

    public RangeSeekBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public RangeSeekBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public RangeSeekBar(Context context, long absoluteMinValuePrim, long absoluteMaxValuePrim, SeekBarType seekBarType) {
        super(context);
        this.absoluteMinValuePrim = absoluteMinValuePrim;
        this.absoluteMaxValuePrim = absoluteMaxValuePrim;
        setFocusable(true);
        setFocusableInTouchMode(true);
        this.seekBarType=seekBarType;
        init();
    }

    private void init() {
        mScaledTouchSlop = ViewConfiguration.get(getContext()).getScaledTouchSlop();
       // thumbWidth = UIUtils.dp2Px(8);
        //System.out.println("trtr dp2px 8 w "+thumbWidth);
        //thumbHeight = UIUtils.dp2Px(62);


        mBitmapBlack = BitmapFactory.decodeResource(getResources(), R.drawable.video_overlay_black);
        mBitmapPro = BitmapFactory.decodeResource(getResources(), R.drawable.video_overlay_trans);
        leftSeekBarImage=BitmapFactory.decodeResource(getResources(), R.mipmap.lower_image_bar_tool_left);
        rightSeekBarImage=BitmapFactory.decodeResource(getResources(), R.mipmap.lower_image_bar_tool_right);
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        rectPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        rectPaint.setStyle(Paint.Style.FILL);
        rectPaint.setColor(Color.parseColor("#E040FB"));
        mGrayPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mGrayPaint.setColor(getContext().getResources().getColor(R.color.cccccc));
        mShadowPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mShadowPaint.setColor(getContext().getResources().getColor(R.color.ff7f000000));
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = 300;
        if (MeasureSpec.UNSPECIFIED != MeasureSpec.getMode(widthMeasureSpec)) {
            width = MeasureSpec.getSize(widthMeasureSpec);
        }
        int height = 120;
        if (MeasureSpec.UNSPECIFIED != MeasureSpec.getMode(heightMeasureSpec)) {
            height = MeasureSpec.getSize(heightMeasureSpec);
        }
        setMeasuredDimension(width, height);
        //System.out.println("trtr seek h "+getHeight());

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        thumbHeight=getHeight();
        thumbWidth=(int )(thumbHeight*thumbRatio);
        thumbHalfWidth = thumbWidth / 2;


        float bg_middle_left = 0;
        float bg_middle_right = getWidth() - getPaddingRight() ;
        float scale = (bg_middle_right - bg_middle_left) / mBitmapPro.getWidth();

        float rangeL = normalizedToScreen(normalizedMinValue);
        float rangeR = normalizedToScreen(normalizedMaxValue);
        float scale_pro = (rangeR - rangeL) / mBitmapPro.getWidth();
        float rangeExceptional=normalizedToScreen(exceptionalLentgh);
        if (scale_pro > 0) {
            try {
                Matrix pro_mx = new Matrix();
                pro_mx.postScale(scale_pro, 1f);
                Bitmap m_bitmap_pro_new = Bitmap.createBitmap(mBitmapPro, 0, 0, mBitmapPro.getWidth(),
                        mBitmapPro.getHeight(), pro_mx, true);

                canvas.drawBitmap(m_bitmap_pro_new, rangeL, thumbPaddingTop, paint);

                Matrix mx = new Matrix();
                mx.postScale(scale, 1f);
                Bitmap m_bitmap_black_new = Bitmap
                    .createBitmap(mBitmapBlack, 0, 0, mBitmapBlack.getWidth(), mBitmapBlack.getHeight(), mx, true);


                if(seekBarType== SeekBarType.LEFT){
                    //System.out.println("draw bar");
                    Bitmap m_bg_new1 = Bitmap
                            .createBitmap(m_bitmap_black_new, 0, 0, (int) (rangeL - bg_middle_left) + (int) thumbWidth / 2, mBitmapBlack.getHeight());
                    canvas.drawBitmap(m_bg_new1, bg_middle_left, thumbPaddingTop, paint);


                    canvas.drawBitmap(Bitmap.createScaledBitmap(rightSeekBarImage,thumbWidth,thumbHeight,true),normalizedToScreen(normalizedMinValue),
                            thumbPaddingTop,paint);
                }else if(seekBarType==seekBarType.RIGHT){
                    Bitmap m_bg_new2 = Bitmap
                            .createBitmap(m_bitmap_black_new, (int) (rangeL - bg_middle_left) , 0, (int) (getWidth()-rangeL) , mBitmapBlack.getHeight());
                    canvas.drawBitmap(m_bg_new2, (int) (rangeL ), thumbPaddingTop, paint);

                    canvas.drawBitmap(Bitmap.createScaledBitmap(leftSeekBarImage,thumbWidth,thumbHeight,true),normalizedToScreen(normalizedMinValue),
                            thumbPaddingTop,paint);

                } else if(seekBarType== SeekBarType.BOTH){
                    Bitmap m_bg_new1 = Bitmap
                            .createBitmap(m_bitmap_black_new, 0, 0, (int) (rangeL - bg_middle_left) + (int) thumbWidth / 2, mBitmapBlack.getHeight());
                    canvas.drawBitmap(m_bg_new1, bg_middle_left, thumbPaddingTop, paint);

                    canvas.drawRect(normalizedToScreen(normalizedMinValue),
                            thumbPaddingTop,
                            normalizedToScreen(normalizedMinValue) + thumbWidth,
                            thumbPaddingTop + thumbHeight, rectPaint); //left white bar

                    Bitmap m_bg_new2 = Bitmap
                            .createBitmap(m_bitmap_black_new, (int) (rangeR - thumbWidth / 2), 0, (int) (getWidth() - rangeR) + (int) thumbWidth / 2, mBitmapBlack.getHeight());
                    canvas.drawBitmap(m_bg_new2, (int) (rangeR - thumbWidth / 2), thumbPaddingTop, paint);

                    canvas.drawRect(normalizedToScreen(normalizedMaxValue) - thumbWidth,
                    thumbPaddingTop,
                    normalizedToScreen(normalizedMaxValue),
                    thumbPaddingTop + thumbHeight, rectPaint); // right white bar
                } else if(seekBarType== SeekBarType.NOTHING){
                    Bitmap m_bg_new2 = Bitmap
                            .createBitmap(m_bitmap_black_new, (int) (rangeL - bg_middle_left) , 0, (int) (getWidth()-rangeL) , mBitmapBlack.getHeight());
                    // canvas.drawBitmap(m_bg_new2, (int) (rangeL ), thumbPaddingTop, paint);

                    canvas.drawBitmap(Bitmap.createScaledBitmap(leftSeekBarImage,thumbWidth,thumbHeight,true),normalizedToScreen(normalizedMinValue),
                            thumbPaddingTop,paint);

                }else if(seekBarType== SeekBarType.EXCEPTIONAL){
                    //System.out.println("exc rangeL= "+rangeL+" rangeR= "+rangeR+" execeptional = "+rangeExceptional +" minvalue= "+normalizedToScreen(normalizedMinValue)+
                         //   " maxValue= "+ normalizedToScreen(normalizedMaxValue)+" width+ "+getWidth());
                    Bitmap m_bg_new1 = Bitmap
                            .createBitmap(m_bitmap_black_new,(int) rangeExceptional, 0, (int) (rangeL - rangeExceptional+thumbWidth/2), mBitmapBlack.getHeight());
                    canvas.drawBitmap(m_bg_new1, rangeExceptional, thumbPaddingTop, paint);

                    canvas.drawBitmap(Bitmap.createScaledBitmap(rightSeekBarImage,thumbWidth,thumbHeight,true),normalizedToScreen(normalizedMinValue),
                            thumbPaddingTop,paint);

                }


            } catch (Exception e) {
                Log.e(TAG,
                        "IllegalArgumentException--width=" + mBitmapPro.getWidth() + "Height=" + mBitmapPro.getHeight()
                                + "scale_pro=" + scale_pro, e);
            }
        }
    }


    public void setMin_cut_time(long min_cut_time) {
        this.min_cut_time = min_cut_time;
    }


    private float normalizedToScreen(double normalizedCoord) {
        return (float) (getPaddingLeft() + normalizedCoord * (getWidth() - getPaddingLeft() - getPaddingRight()));
    }

    private double valueToNormalized(long value) {
        if (0 == absoluteMaxValuePrim - absoluteMinValuePrim) {
            return 0d;
        }
        return (value - absoluteMinValuePrim) / (absoluteMaxValuePrim - absoluteMinValuePrim);
    }

    public void setSelectedMinValue(long value) {
        if (0 == (absoluteMaxValuePrim - absoluteMinValuePrim)) {
            setNormalizedMinValue(0d);
        } else {
            setNormalizedMinValue(valueToNormalized(value));
        }
    }

    public void setSelectedMaxValue(long value) {
        if (0 == (absoluteMaxValuePrim - absoluteMinValuePrim)) {
            setNormalizedMaxValue(1d);
        } else {
            setNormalizedMaxValue(valueToNormalized(value));
        }
    }

    public void setNormalizedMinValue(double value) {
        normalizedMinValue = Math.max(0d, Math.min(1d, Math.min(value, normalizedMaxValue)));
        invalidate();
    }


    public void setNormalizedMaxValue(double value) {
        normalizedMaxValue = Math.max(0d, Math.min(1d, Math.max(value, normalizedMinValue)));
        invalidate();
    }


    public long getSelectedMinValue() {
        return normalizedToValue(normalizedMinValueTime);
    }

    public long getSelectedMaxValue() {
        return normalizedToValue(normalizedMaxValueTime);
    }

    private long normalizedToValue(double normalized) {
        return (long) (absoluteMinValuePrim + normalized
                * (absoluteMaxValuePrim - absoluteMinValuePrim));
    }

    public void setNotifyWhileDragging(boolean flag) {
        this.notifyWhileDragging = flag;
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        final Bundle bundle = new Bundle();
        bundle.putParcelable("SUPER", super.onSaveInstanceState());
        bundle.putDouble("MIN", normalizedMinValue);
        bundle.putDouble("MAX", normalizedMaxValue);
        bundle.putDouble("MIN_TIME", normalizedMinValueTime);
        bundle.putDouble("MAX_TIME", normalizedMaxValueTime);
        return bundle;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable parcel) {
        final Bundle bundle = (Bundle) parcel;
        super.onRestoreInstanceState(bundle.getParcelable("SUPER"));
        normalizedMinValue = bundle.getDouble("MIN");
        normalizedMaxValue = bundle.getDouble("MAX");
        normalizedMinValueTime = bundle.getDouble("MIN_TIME");
        normalizedMaxValueTime = bundle.getDouble("MAX_TIME");
    }

    private OnRangeSeekBarChangeListener listener;

    public interface OnRangeSeekBarChangeListener {
        void onRangeSeekBarValuesChanged(RangeSeekBar bar, long minValue, long maxValue, int action,
                                         boolean isMin, Thumb pressedThumb);
    }

    public void setOnRangeSeekBarChangeListener(OnRangeSeekBarChangeListener listener) {
        this.listener = listener;
    }

    public SeekBarType getSeekBarType() {
        return seekBarType;
    }

    public void setSeekBarType(SeekBarType seekBarType) {
        this.seekBarType = seekBarType;
    }

    public void setThumbRatio(double thumbRatio){
        this.thumbRatio=thumbRatio;
    }

    public int getThumbWidth(){
        return  thumbWidth;
    }
}

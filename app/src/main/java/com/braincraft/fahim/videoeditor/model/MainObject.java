package com.braincraft.fahim.videoeditor.model;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.braincraft.fahim.videoeditor.adapter.TrimVideoAdapter;
import com.braincraft.fahim.videoeditor.utils.ExtractFrameWorkThread;
import com.braincraft.fahim.videoeditor.view.RangeSeekBar;

import static com.braincraft.fahim.videoeditor.activity.MyTrimVideoActivity.MARGIN_THUMB;
import static com.braincraft.fahim.videoeditor.activity.MyTrimVideoActivity.setTimeToTextView;

public abstract class MainObject {

     long duration;
     float averageMsPx;
    protected long leftProgress;
    protected long rightProgress;
     long leftStart;
     long rightStart;
    protected long leftProgressRight;
     long rightProgressRight;
     long scrollPos = 0;
     int mScaledTouchSlop;
     int lastScrollX;
     MediaPlayer mMediaPlayer;
     boolean canShow=false,canShowRight=false,isLeftBarShowing=false,isRightBarShowing=false,firstTimeRight=true;
     long middleValue,leftBarOffset;
     double time,thumbOffsetRatio=200/(double)MARGIN_THUMB;
     int mediaPlayerTime;
     long leftBarValue;
     long rightBarValue;
    boolean isSeekingOnce=false;
    boolean isOverScaledTouchSlop;


    RecyclerView mRecyclerView;
    ImageView startHereTv,endHereTv;
    LinearLayout overlay,seekBarLayout,seekBarLayoutRight;
    protected RangeSeekBar seekBar;
    protected RangeSeekBar seekBarRight;
    double startSecond,endSecond;
    protected Context context;
    TextView startTimeTv,endTimeTv,mTvShootTipMiddle;
    
    public MainObject(Context context, RecyclerView mRecyclerView, ImageView startHereTv, ImageView endHereTv, LinearLayout overlay, 
                      TextView startTimeTv, TextView endTimeTv,LinearLayout seekBarLayout,LinearLayout seekBarLayoutRight,TextView mTvShootTipMiddle) {
        this.context=context;
        this.mRecyclerView=mRecyclerView;
        this.startHereTv =startHereTv;
        this.endHereTv=endHereTv;
        this.overlay=overlay;
        this.startTimeTv=startTimeTv;
        this.endTimeTv=endTimeTv;
        this.seekBarLayout=seekBarLayout;
        this.seekBarLayoutRight=seekBarLayoutRight;
        this.mTvShootTipMiddle=mTvShootTipMiddle;
        
    }

    public abstract void initEditVideo(long duration);

    public abstract void setLeftBarOffset(long leftBarOffset);


     int getScrollXDistance() {
        LinearLayoutManager layoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        int position = layoutManager.findFirstVisibleItemPosition();
        View firstVisibleChildView = layoutManager.findViewByPosition(position);
        int itemWidth = firstVisibleChildView.getWidth();
        return (position) * itemWidth - firstVisibleChildView.getLeft();
    }

    public abstract  void setScrollListener();

    public void setMiddleValue(long middleValue) {
        this.middleValue=middleValue;
    }

    public void setMediaPlayer(MediaPlayer mMediaPlayer) {
        this.mMediaPlayer=mMediaPlayer;
    }

    public void setAverageMsPx(float averageMsPx) {
        this.averageMsPx=averageMsPx;
    }

    public void resetValues() {
        leftProgress=0;
        rightProgress=0;
        leftStart=0;
        rightStart=0;
        leftProgressRight=0;
        scrollPos=0;
        canShow=false;
        canShowRight=false;
        lastScrollX=0;
        isLeftBarShowing=false;
        isRightBarShowing=false;
        firstTimeRight=true;
        leftBarValue=0;
        rightBarValue=0;
        isSeekingOnce=false;
        lastScrollX=0;
        mediaPlayerTime=0;
        leftProgressRight=0;
        rightProgressRight=0;
        isOverScaledTouchSlop=false;
        time=0;
        startSecond=0;endSecond=0;
    }

    public abstract void removeSeekBars();

    public abstract void removeListener();


     void setImageToIvAndEnableDisable(ImageView iv, boolean isEnableDisable,int mipmap){
        iv.setEnabled(isEnableDisable);
        iv.setImageResource(mipmap);
    }

    public void setTimesToTextView(){
        setTimeToTextView(startTimeTv, time);
        setTimeToTextView(endTimeTv, time);
    }

    public abstract void setImageToStartHereTv();

    public  double getStartSecond(){
        return startSecond;
    }

    public double getEndSecond() {
        return endSecond;
    }

    public void setDuration(long duration) {
        this.duration=duration;
    }
}

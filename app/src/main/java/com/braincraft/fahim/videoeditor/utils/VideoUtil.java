package com.braincraft.fahim.videoeditor.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;

import com.braincraft.fahim.videoeditor.activity.VideoAlbumActivity;
import com.braincraft.fahim.videoeditor.base.BaseActivity;
import com.braincraft.fahim.videoeditor.model.LocalVideoModel;
import com.coremedia.iso.boxes.Container;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
import com.googlecode.mp4parser.authoring.tracks.AppendTrack;
import com.googlecode.mp4parser.authoring.tracks.CroppedTrack;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


@SuppressWarnings("ResultOfMethodCallIgnored")
public class VideoUtil {

    private static final String TAG = VideoUtil.class.getSimpleName();
    public static final String POSTFIX = ".jpeg";
    private static final String TRIM_PATH = "small_video";
    private static final String THUMB_PATH = "thumb";

    public static Observable<String> cutVideo(final String src, final String dest, final double startSec, final double endSec) {

        return Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(ObservableEmitter<String> emitter) {
                try {
                    double startSecond = startSec;
                    double endSecond = endSec;
                    Movie movie = MovieCreator.build(src);
                    List<Track> tracks = movie.getTracks();
                    movie.setTracks(new ArrayList<Track>());

                    boolean timeCorrected = false;
                    // Here we try to find a track that has sync samples. Since we can only start decoding
                    // at such a sample we SHOULD make sure that the start of the new fragment is exactly
                    // such a frame
                    for (Track track : tracks) {
                        if (track.getSyncSamples() != null && track.getSyncSamples().length > 0) {
                            if (timeCorrected) {
                                // This exception here could be a false positive in case we have multiple tracks
                                // with sync samples at exactly the same positions. E.g. a single movie containing
                                // multiple qualities of the same video (Microsoft Smooth Streaming file)

                                throw new RuntimeException(
                                    "The startTime has already been corrected by another track with SyncSample. Not Supported.");
                            }
                            startSecond = correctTimeToSyncSample(track, startSecond, false);
                            endSecond = correctTimeToSyncSample(track, endSecond, true);

                            timeCorrected = true;
                        }
                    }

                    //Log.e(TAG, "startSecond:" + startSecond + ", endSecond:" + endSecond);

                    if (endSecond - startSecond > 10) {
                        int duration = (int) (endSec - startSec);
                        endSecond = startSecond + duration;
                    }
                    //fix bug: 部分视频裁剪后endSecond=0.0,导致播放失败
                    if (endSecond == 0.0) {
                        int duration = (int) (endSec - startSec);
                        endSecond = startSecond + duration;
                    }

                    for (Track track : tracks) {
                        long currentSample = 0;
                        double currentTime = 0;
                        double lastTime = -1;
                        long startSample = -1;
                        long endSample = -1;

                        for (int i = 0; i < track.getSampleDurations().length; i++) {
                            long delta = track.getSampleDurations()[i];

                            if (currentTime > lastTime && currentTime <= startSecond) {
                                // current sample is still before the new starttime
                                startSample = currentSample;
                            }
                            if (currentTime > lastTime && currentTime <= endSecond) {
                                // current sample is after the new start time and still before the new endtime
                                endSample = currentSample;
                            }

                            lastTime = currentTime;
                            currentTime +=
                                (double) delta / (double) track.getTrackMetaData().getTimescale();
                            currentSample++;
                        }
                        //Log.e(TAG, "startSample:" + startSample + ", endSample:" + endSample);
                        movie.addTrack(new CroppedTrack(track, startSample, endSample));

                        Container out = new DefaultMp4Builder().build(movie);
                        FileOutputStream fos = new FileOutputStream(String.format(dest));
                        FileChannel fc = fos.getChannel();
                        out.writeContainer(fc);

                        fc.close();
                        fos.close();
                    }

                    emitter.onNext(dest);

                } catch (Exception e) {
                    emitter.onError(e);
                }
                emitter.onComplete();
            }
        })
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<String> cutVideo(final String src, final String dest, final double startSec, final double endSec,final double lastStartSec,final  double lastEndSec) {

        return Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(ObservableEmitter<String> emitter) {
                try {
                    Movie movie1 = getMovie(startSec, endSec, src);
                    Movie movie2 = getMovie(lastStartSec, lastEndSec, src);

                    Movie finalMovie=appendVideo(new Movie[]{movie1,movie2});

                    Container out = new DefaultMp4Builder().build(finalMovie);
                    FileOutputStream fos = new FileOutputStream(String.format(dest));
                    FileChannel fc = fos.getChannel();
                    out.writeContainer(fc);

                    fc.close();
                    fos.close();

                    emitter.onNext(dest);

                } catch (Exception e) {
                    emitter.onError(e);
                }
                emitter.onComplete();
            }
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * @param
     * @return 	return combine video path string
     * */
    private static Movie appendVideo(Movie[] inMovies) throws IOException{

        List<Track> videoTracks = new LinkedList<Track>();
        List<Track> audioTracks = new LinkedList<Track>();
        for (Movie m : inMovies) {
            for (Track t : m.getTracks()) {
                if (t.getHandler().equals("soun")) {
                    audioTracks.add(t);
                }
                if (t.getHandler().equals("vide")) {
                    videoTracks.add(t);
                }
            }
        }

        Movie result = new Movie();
        //Log.v(TAG, "audioTracks size = " + audioTracks.size()
            //    + " videoTracks size = " + videoTracks.size());
        if (audioTracks.size() > 0) {
            result.addTrack(new AppendTrack(audioTracks.toArray(new Track[audioTracks.size()])));
        }
        if (videoTracks.size() > 0) {
            result.addTrack(new AppendTrack(videoTracks.toArray(new Track[videoTracks.size()])));
        }

        return result;
    }

    private static Movie getMovie(double startSec, double endSec, String src) throws IOException {
        double startSecond = startSec;
        double endSecond = endSec;
        Movie movie = MovieCreator.build(src);
        List<Track> tracks = movie.getTracks();
        movie.setTracks(new ArrayList<Track>());

        boolean timeCorrected = false;
        // Here we try to find a track that has sync samples. Since we can only start decoding
        // at such a sample we SHOULD make sure that the start of the new fragment is exactly
        // such a frame
        for (Track track : tracks) {
            if (track.getSyncSamples() != null && track.getSyncSamples().length > 0) {
                if (timeCorrected) {
                    // This exception here could be a false positive in case we have multiple tracks
                    // with sync samples at exactly the same positions. E.g. a single movie containing
                    // multiple qualities of the same video (Microsoft Smooth Streaming file)

                    throw new RuntimeException(
                            "The startTime has already been corrected by another track with SyncSample. Not Supported.");
                }
                startSecond = correctTimeToSyncSample(track, startSecond, false);
                endSecond = correctTimeToSyncSample(track, endSecond, true);

                timeCorrected = true;
            }
        }

        //Log.e(TAG, "startSecond:" + startSecond + ", endSecond:" + endSecond);

        if (endSecond - startSecond > 10) {
            int duration = (int) (endSec - startSec);
            endSecond = startSecond + duration;
        }
        if (endSecond == 0.0) {
            int duration = (int) (endSec - startSec);
            endSecond = startSecond + duration;
        }

        for (Track track : tracks) {
            long currentSample = 0;
            double currentTime = 0;
            double lastTime = -1;
            long startSample = -1;
            long endSample = -1;

            for (int i = 0; i < track.getSampleDurations().length; i++) {
                long delta = track.getSampleDurations()[i];

                if (currentTime > lastTime && currentTime <= startSecond) {
                    // current sample is still before the new starttime
                    startSample = currentSample;
                }
                if (currentTime > lastTime && currentTime <= endSecond) {
                    // current sample is after the new start time and still before the new endtime
                    endSample = currentSample;
                }

                lastTime = currentTime;
                currentTime +=
                        (double) delta / (double) track.getTrackMetaData().getTimescale();
                currentSample++;
            }
            //Log.e(TAG, "startSample:" + startSample + ", endSample:" + endSample);
            movie.addTrack(new CroppedTrack(track, startSample, endSample));


        }
        return movie;
    }

    private static double correctTimeToSyncSample(Track track, double cutHere, boolean next) {
        double[] timeOfSyncSamples = new double[track.getSyncSamples().length];
        long currentSample = 0;
        double currentTime = 0;
        for (int i = 0; i < track.getSampleDurations().length; i++) {
            long delta = track.getSampleDurations()[i];

            if (Arrays.binarySearch(track.getSyncSamples(), currentSample + 1) >= 0) {
                timeOfSyncSamples[Arrays
                    .binarySearch(track.getSyncSamples(), currentSample + 1)] = currentTime;
            }
            currentTime += (double) delta / (double) track.getTrackMetaData().getTimescale();
            currentSample++;

        }
        double previous = 0;
        for (double timeOfSyncSample : timeOfSyncSamples) {
            if (timeOfSyncSample > cutHere) {
                if (next) {
                    return timeOfSyncSample;
                } else {
                    return previous;
                }
            }
            previous = timeOfSyncSample;
        }
        return timeOfSyncSamples[timeOfSyncSamples.length - 1];
    }


    public static String getTrimmedVideoPath(Context context, String dirName, String fileNamePrefix) {
        String finalPath = "";
        String dirPath = "";
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            dirPath = context.getExternalCacheDir() + File.separator + dirName; // /mnt/sdcard/Android/data/<package name>/files/...
        } else {
            dirPath = context.getCacheDir() + File.separator + dirName; // /data/data/<package name>/files/...
        }

        dirPath=Environment.getExternalStorageDirectory()+dirName;
        File file = new File(dirPath);
        if (!file.exists()) {
            file.mkdirs();
        }
        finalPath = file.getAbsolutePath();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault())
            .format(new Date());
        String outputName = fileNamePrefix + timeStamp + ".mp4";
        finalPath = finalPath + "/" + outputName;
        return finalPath;
    }

    public static String getTrimmedVideoDir(Context context, String dirName) {
        String dirPath = "";
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            dirPath = context.getExternalCacheDir() + File.separator
                + dirName; // /mnt/sdcard/Android/data/<package name>/files/...
        } else {
            dirPath = context.getCacheDir() + File.separator
                + dirName; // /data/data/<package name>/files/...
        }
        File file = new File(dirPath);
        if (!file.exists()) {
            file.mkdirs();
        }
        return dirPath;
    }



    public static String saveImageToSDForEdit(Bitmap bmp, String dirPath, String fileName) {
        if (bmp == null) {
            return "";
        }
        File appDir = new File(dirPath);
        if (!appDir.exists()) {
            appDir.mkdir();
        }
        File file = new File(appDir, fileName);
        try {
            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 80, fos);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file.getAbsolutePath();
    }

    public static void deleteFile(File f) {
        if (f.isDirectory()) {
            File[] files = f.listFiles();
            if (files != null && files.length > 0) {
                for (int i = 0; i < files.length; ++i) {
                    deleteFile(files[i]);
                }
            }
        }
        f.delete();
    }

    public static String getSaveEditThumbnailDir(Context context) {
        String state = Environment.getExternalStorageState();
        File rootDir =
            state.equals(Environment.MEDIA_MOUNTED) ? context.getExternalCacheDir()
                : context.getCacheDir();
        File folderDir = new File(rootDir.getAbsolutePath() + File.separator + TRIM_PATH + File.separator + THUMB_PATH);
        if (folderDir == null) {
            folderDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                + File.separator + "videoeditor" + File.separator + "picture");
        }
        if (!folderDir.exists() && folderDir.mkdirs()) {

        }
        return folderDir.getAbsolutePath();
    }

    public static Observable<ArrayList<LocalVideoModel>> getLocalVideoFiles(final Context context) {

        return Observable.create(new ObservableOnSubscribe<ArrayList<LocalVideoModel>>() {
            @Override
            public void subscribe(ObservableEmitter<ArrayList<LocalVideoModel>> emitter) {
                ArrayList<LocalVideoModel> videoModels = new ArrayList<>();
                ContentResolver resolver = context.getContentResolver();
                try {
                    Cursor cursor = resolver
                            .query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, null,
                                    null, null, MediaStore.Video.Media.DATE_MODIFIED + " desc");
                    if (cursor != null) {
                        while (cursor.moveToNext()) {
                            LocalVideoModel video = new LocalVideoModel();
                            if (cursor
                                    .getLong(cursor.getColumnIndex(MediaStore.Video.Media.DURATION))
                                    != 0) {
                                video.setDuration(
                                        cursor.getLong(
                                                cursor.getColumnIndex(MediaStore.Video.Media.DURATION)));
                                video.setVideoPath(
                                        cursor.getString(
                                                cursor.getColumnIndex(MediaStore.Video.Media.DATA)));
                                video.setCreateTime(cursor
                                        .getString(
                                                cursor.getColumnIndex(MediaStore.Video.Media.DATE_ADDED)));
                                video.setVideoName(cursor
                                        .getString(cursor
                                                .getColumnIndex(MediaStore.Video.Media.DISPLAY_NAME)));
                                videoModels.add(video);
                            }
                        }
                        emitter.onNext(videoModels);
                        cursor.close();
                    }
                } catch (Exception e) {
                    emitter.onError(e);
                }
                emitter.onComplete();
            }
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static String convertSecondsToTime(long seconds) {
        String timeStr = null;
        int hour = 0;
        int minute = 0;
        int second = 0;
        if (seconds <= 0)
            return "00:00";
        else {
            minute = (int) seconds / 60;
            if (minute < 60) {
                second = (int) seconds % 60;
                timeStr = unitFormat(minute) + ":" + unitFormat(second);
            } else {
                hour = minute / 60;
                if (hour > 99)
                    return "99:59:59";
                minute = minute % 60;
                second = (int) (seconds - hour * 3600 - minute * 60);
                timeStr = unitFormat(hour) + ":" + unitFormat(minute) + ":" + unitFormat(second);
            }
        }
        return timeStr;
    }
    public static String unitFormat(int i) {
        String retStr = null;
        if (i >= 0 && i < 10)
            retStr = "0" + Integer.toString(i);
        else
            retStr = "" + i;
        return retStr;
    }

    public static String getVideoFilePath(String url) {

        if (TextUtils.isEmpty(url) || url.length() < 5)
            return "";

        if (url.substring(0, 4).equalsIgnoreCase("http")) {
        } else
            url = "file://" + url;

        return url;
    }
}

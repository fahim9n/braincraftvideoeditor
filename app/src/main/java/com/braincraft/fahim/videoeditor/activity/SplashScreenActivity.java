package com.braincraft.fahim.videoeditor.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.braincraft.fahim.videoeditor.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public class SplashScreenActivity extends AppCompatActivity {
    String path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                String videoName="test10.mp4";

                //System.out.println("video path a");
                File f = new File(getCacheDir()+"/"+videoName);
                if (!f.exists()) try {


                    InputStream is = getAssets().open(videoName);
                    int size = is.available();
                    byte[] buffer = new byte[size];
                    is.read(buffer);
                    is.close();


                    FileOutputStream fos = new FileOutputStream(f);
                    fos.write(buffer);
                    fos.close();

                    //System.out.println("video path b");

                } catch (Exception e) { throw new RuntimeException(e); }

                path=f.getPath();

                startHomeActivity();
            }
        },1000);




    }

    private void startHomeActivity() {
        Intent homeIntent = new Intent(SplashScreenActivity.this, MyTrimVideoActivity.class);
        homeIntent.putExtra("videoPath",path);
        startActivity(homeIntent);
        finishAffinity();
    }
}

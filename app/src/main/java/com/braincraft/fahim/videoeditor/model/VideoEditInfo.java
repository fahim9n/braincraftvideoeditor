package com.braincraft.fahim.videoeditor.model;

import java.io.Serializable;


public class VideoEditInfo implements Serializable {

    public String path;
    public long time;

    public VideoEditInfo() {
    }


    @Override
    public String toString() {
        return "VideoEditInfo{" +
            "path='" + path + '\'' +
            ", time='" + time + '\'' +
            '}';
    }
}

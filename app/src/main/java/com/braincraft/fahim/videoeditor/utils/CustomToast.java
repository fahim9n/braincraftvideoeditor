package com.braincraft.fahim.videoeditor.utils;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.braincraft.fahim.videoeditor.R;

public class CustomToast extends Toast {
    Context context;
    Toast toast;
    View layout;
    TextView descriptionTextView,titleTextView;

    public CustomToast(Context context) {
        super(context);
        this.context=context;
        createToast();

    }

    private void createToast() {
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layout = inflater.inflate(R.layout.toast_layout,null);


        titleTextView = (TextView) layout.findViewById(R.id.title_tv);
        descriptionTextView = (TextView) layout.findViewById(R.id.desc_tv);
    }

    public void showToast(String title, String desc, int length){
        toast = new Toast(context);
        titleTextView.setText(title);
        descriptionTextView.setText(desc);
        toast.setGravity(Gravity.BOTTOM, 0, 200);
        toast.setDuration(length);
        toast.setView(layout);
        toast.show();
    }


}

package com.braincraft.fahim.videoeditor.model;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.braincraft.fahim.videoeditor.R;
import com.braincraft.fahim.videoeditor.utils.SeekBarType;
import com.braincraft.fahim.videoeditor.utils.UIUtils;
import com.braincraft.fahim.videoeditor.utils.VideoUtil;
import com.braincraft.fahim.videoeditor.view.RangeSeekBar;

import static com.braincraft.fahim.videoeditor.activity.MyTrimVideoActivity.MARGIN;
import static com.braincraft.fahim.videoeditor.activity.MyTrimVideoActivity.MARGIN_LEFT;
import static com.braincraft.fahim.videoeditor.activity.MyTrimVideoActivity.MAX_COUNT_RANGE;
import static com.braincraft.fahim.videoeditor.activity.MyTrimVideoActivity.MAX_CUT_DURATION;
import static com.braincraft.fahim.videoeditor.activity.MyTrimVideoActivity.MIN_CUT_DURATION;
import static com.braincraft.fahim.videoeditor.activity.MyTrimVideoActivity.setTimeToTextView;

public class CutObject extends MainObject {
    public CutObject(Context context, RecyclerView mRecyclerView, ImageView startHereTv, ImageView endHereTv, LinearLayout overlay, TextView startTimeTv, TextView endTimeTv, LinearLayout seekBarLayout, LinearLayout seekBarLayoutRight, TextView mTvShootTipMiddle) {
        super(context, mRecyclerView, startHereTv, endHereTv, overlay, startTimeTv, endTimeTv, seekBarLayout, seekBarLayoutRight, mTvShootTipMiddle);
    }

    @Override
    public void  initEditVideo(long duration) {
        setImageToIvAndEnableDisable(endHereTv,false,R.mipmap.end_here_cut_inactive);

        long endPosition = duration;
        boolean isOver_10_s = identifyDuration(duration);
        initSeekBar(endPosition, isOver_10_s);
        initSeekBarRight(endPosition, isOver_10_s);
        setDefualtLeftAndRightProgress(endPosition, isOver_10_s);
        startHereTv.setOnClickListener(getStartHereOnClickListener());
        endHereTv.setOnClickListener(getEndHereOnClickListener());
        setDefaultVisibility();
        new Handler().postDelayed(getRunnable(duration),50);
        setThumbRatioToSeekBars();
    }

    private void setDefaultVisibility() {
        seekBar.setVisibility(View.INVISIBLE);
        seekBarRight.setVisibility(View.INVISIBLE);
    }

    private void setThumbRatioToSeekBars() {
        BitmapDrawable d= (BitmapDrawable) context.getResources().getDrawable(R.mipmap.lower_image_bar_tool_left);
        double thumbRatio= d.getBitmap().getWidth()/(double)d.getBitmap().getHeight();
        seekBar.setThumbRatio(thumbRatio);
        seekBarRight.setThumbRatio(thumbRatio);
    }

    private void setDefualtLeftAndRightProgress(long endPosition, boolean isOver_10_s) {
        leftProgress = 0;
        leftProgressRight=0;
        if (isOver_10_s) {
            rightProgress = MAX_CUT_DURATION;
        } else {
            rightProgress = endPosition;
        }
    }

    private void initSeekBarRight(long endPosition, boolean isOver_10_s) {
        //init seekBar right
        if (isOver_10_s) {
            seekBarRight = new RangeSeekBar(context, 0L, MAX_CUT_DURATION, SeekBarType.LEFT);
            seekBarRight.setSelectedMaxValue(MAX_CUT_DURATION);
        } else {
            seekBarRight = new RangeSeekBar(context, 0L, endPosition, SeekBarType.LEFT);
            seekBarRight.setSelectedMaxValue(endPosition);
        }
        seekBarRight.setMin_cut_time(MIN_CUT_DURATION);
        seekBarRight.setNotifyWhileDragging(true);
        seekBarLayoutRight.addView(seekBarRight);
    }

    private void initSeekBar(long endPosition, boolean isOver_10_s) {
        if (isOver_10_s) {
            seekBar = new RangeSeekBar(context, 0L, MAX_CUT_DURATION, SeekBarType.RIGHT);
            seekBar.setSelectedMaxValue(MAX_CUT_DURATION);
        } else {
            seekBar = new RangeSeekBar(context, 0L, endPosition, SeekBarType.RIGHT);
            seekBar.setSelectedMaxValue(endPosition);
        }
        seekBar.setMin_cut_time(MIN_CUT_DURATION);
        seekBar.setNotifyWhileDragging(true);
        seekBarLayout.addView(seekBar);
    }

    private View.OnClickListener getStartHereOnClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seekBar.setSeekBarType(SeekBarType.RIGHT);
                seekBar.setSelectedMinValue(middleValue-leftBarOffset);
                leftBarValue=middleValue-leftBarOffset;
                canShow=true;
                if(!isSeekingOnce) {
                    leftProgress = -5000;
                }
                leftStart=middleValue-(-leftProgress) - leftBarOffset;
                overlay.setVisibility(View.INVISIBLE);
                seekBar.setVisibility(View.VISIBLE);
                startSecond=time;
                setTimeToTextView(startTimeTv,startSecond);
                isLeftBarShowing=true;

                if(isRightBarShowing ){
                    seekBar.setSeekBarType(SeekBarType.NOTHING);
                    seekBar.setSelectedMinValue(middleValue-leftBarOffset);

                    seekBarRight.setExceptionalLentgh(middleValue-leftBarOffset);
                    seekBarRight.setSeekBarType(SeekBarType.EXCEPTIONAL);
                    seekBarRight.setSelectedMinValue(rightBarValue);
                }
                setImageToIvAndEnableDisable(endHereTv,true,R.mipmap.end_here_cut);

            }
        };
    }

    private Runnable getRunnable(long duration) {
        return new Runnable() {
            @Override
            public void run() {
                ((Activity)context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //System.out.println("-------haha haha");
                        seekBar.setSeekBarType(SeekBarType.RIGHT);
                        long factor=(long) ((((float)(middleValue-(middleValue+4000))/middleValue))*MARGIN_LEFT*7);
                        //System.out.println("-------haha haha left bar set runnable "+ (middleValue+4000-leftBarOffset+factor));
                        seekBar.setSelectedMinValue(middleValue+4000-leftBarOffset+factor);
                        canShow=true;
                        //System.out.println("ve leftstart first "+ leftProgress);
                        leftStart=4000 - (leftBarOffset);
                        leftBarValue=middleValue+4000-leftBarOffset+factor;

                        overlay.setVisibility(View.INVISIBLE);
                        seekBar.setVisibility(View.VISIBLE);
                        startSecond=2;
                        setTimeToTextView(startTimeTv,startSecond);
                        endSecond=(duration/1000)-2;
                        setTimeToTextView(endTimeTv,endSecond);
                        isLeftBarShowing=true;

                        //endHereTv.setEnabled(true);
                        //endHereTv.setImageResource(R.mipmap.end_here_cut);

                    }
                });
            }
        };
    }

    private View.OnClickListener getEndHereOnClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view.isEnabled()){
                    seekBarRight.setSeekBarType(SeekBarType.LEFT);
                    seekBarRight.setSelectedMinValue(middleValue);
                    rightBarValue=middleValue;
                    canShowRight=true;
                    if(!isSeekingOnce) {
                        leftProgressRight = -5000;
                    }
                    rightStart=middleValue-(-leftProgressRight);
                    //leftStart=middleValue-(-leftProgress) + leftBarOffset;
                    overlay.setVisibility(View.INVISIBLE);
                    seekBarRight.setVisibility(View.VISIBLE);
                    endSecond=time;
                    setTimeToTextView(endTimeTv,endSecond);
                    isRightBarShowing=true;

                    if(isLeftBarShowing ){
                        seekBar.setSeekBarType(SeekBarType.NOTHING);
                        //System.out.println("-------haha haha left bar set onclick right click");
                        seekBar.setSelectedMinValue(leftBarValue);

                        seekBarRight.setExceptionalLentgh(leftBarValue);
                        seekBarRight.setSeekBarType(SeekBarType.EXCEPTIONAL);
                        seekBarRight.setSelectedMinValue(middleValue);
                    }
                    setImageToIvAndEnableDisable(startHereTv,true,R.mipmap.start_here_cut);
                }
            }
        };
    }

    private boolean identifyDuration(long duration) {
        boolean isOver_10_s = false;
        if (duration >= MAX_CUT_DURATION) {
            isOver_10_s = true;
        }
        return isOver_10_s;
    }

    @Override
    public void setLeftBarOffset(long leftBarOffset) {
        this.leftBarOffset = leftBarOffset;
    }

    @Override
    public void setScrollListener() {
        mRecyclerView.scrollToPosition(0);
        mRecyclerView.addOnScrollListener(mOnScrollListenerCut);
    }

    @Override
    public void removeSeekBars() {
        seekBarLayout.removeView(seekBar);
        seekBarLayoutRight.removeView(seekBarRight);
    }

    @Override
    public void removeListener() {
        mRecyclerView.removeOnScrollListener(mOnScrollListenerCut);
    }

    private  RecyclerView.OnScrollListener mOnScrollListenerCut = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            //Log.d(TAG, "-------newState:>>>>>" + newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int scrollX = getScrollXDistance();
            boolean leftGoneThroughRight=false,rightGoneThroughRight=false;
            //System.out.println("scrollx "+scrollX);
            if (scrollX == -MARGIN) {
                scrollPos = 0;
            } else {
                if(!isSeekingOnce){
                    isSeekingOnce=true;
                }
                calculateLeftProgress(scrollX);
                setPositionToMediaPlayer();
                setCurrentTime();

                setInitialEndSeekBar();

                long val=leftProgress-leftStart;
                //System.out.println("-------haha haha left start scroll "+ leftStart+ " left progress "+leftProgress);
                long factor=(long) ((((float)(middleValue-(-val))/middleValue))*MARGIN_LEFT*7);
                //System.out.println("-------haha haha left bar val "+val+ " factor "+factor+ " minus "+ (float)(middleValue-(-val))/middleValue);
                leftBarValue=(-(val) + factor );
                leftGoneThroughRight = setLeftseekBarVisibility(leftGoneThroughRight, val);

                leftProgressRight = seekBarRight.getSelectedMinValue() + scrollPos;
                rightProgressRight = seekBarRight.getSelectedMaxValue() + scrollPos;
                long valRight=leftProgressRight-rightStart;
                long factorRight=(long) ((((float)(middleValue-(-valRight))/middleValue))*MARGIN_LEFT*7);
                rightBarValue=(-(valRight) +factorRight);
                //System.out.println("rightbar value "+rightBarValue+" "+rightStart);


                rightGoneThroughRight = setRightSeekBarVisibility(rightGoneThroughRight, valRight);

                setButtonEnableDisable(val, factor, valRight, factorRight);

                setBothSeekBar();

                if(leftGoneThroughRight && rightGoneThroughRight){
                    overlay.setVisibility(View.INVISIBLE);
                }


            }
            lastScrollX = scrollX;
        }
    };

    private void setBothSeekBar() {
        if(isLeftBarShowing && isRightBarShowing){
            //System.out.println("hula left "+leftBarValue+ " right"+rightBarValue);
            seekBarRight.setExceptionalLentgh(leftBarValue);
            seekBarRight.setSeekBarType(SeekBarType.EXCEPTIONAL);
            seekBar.setSeekBarType(SeekBarType.NOTHING);
        }
    }

    private boolean setRightSeekBarVisibility(boolean rightGoneThroughRight, long valRight) {
        if((valRight)>leftBarOffset ){
            if(canShowRight){
                seekBarRight.setVisibility(View.INVISIBLE);
                isRightBarShowing=false;

                overlay.setVisibility(View.INVISIBLE);
                //System.out.println("visibility right no >");

            }


        }else if((valRight)< -(middleValue*2)-(leftBarOffset/2)){
            if(canShowRight){
                if(!isLeftBarShowing){
                    overlay.setVisibility(View.VISIBLE);

                }
                //System.out.println("visibility right yes");

            }
            //overlay.setVisibility(View.VISIBLE);
            seekBarRight.setVisibility(View.INVISIBLE);
            isRightBarShowing=false;
            rightGoneThroughRight=true;
        }else{
            if(canShowRight){
                overlay.setVisibility(View.INVISIBLE);
                //System.out.println("visibility right no normal");
                seekBarRight.setSeekBarType(SeekBarType.LEFT);
                seekBarRight.setSelectedMinValue( rightBarValue);
                seekBarRight.setVisibility(View.VISIBLE);
                isRightBarShowing=true;
            }

        }
        return rightGoneThroughRight;
    }

    private boolean setLeftseekBarVisibility(boolean leftGoneThroughRight, long val) {
        if((val)>leftBarOffset ){
            if(canShow){
                if(!isRightBarShowing){
                    overlay.setVisibility(View.VISIBLE);
                }
                //System.out.println("visibility left yes");

                seekBar.setVisibility(View.INVISIBLE);
                isLeftBarShowing=false;
            }


        }else if((val)< -((middleValue*2)+(leftBarOffset/2))){
            overlay.setVisibility(View.INVISIBLE);
            //System.out.println("visibility left no");
            seekBar.setVisibility(View.INVISIBLE);
            isLeftBarShowing=false;
            leftGoneThroughRight=true;

        }else{
            if(canShow){
                overlay.setVisibility(View.INVISIBLE);
                //System.out.println("visibility left no");
                seekBar.setSeekBarType(SeekBarType.RIGHT);
                //System.out.println("-------haha haha left bar set scroll "+leftBarValue);
                seekBar.setSelectedMinValue(leftBarValue );
                seekBar.setVisibility(View.VISIBLE);
                isLeftBarShowing=true;
            }

        }
        return leftGoneThroughRight;
    }

    private void setInitialEndSeekBar() {
        double durationInSecond=duration/1000;
        if(durationInSecond-time<=4.5 && durationInSecond-time >0){
            if(firstTimeRight && !canShowRight){
                //System.out.println("jajaj ");
                firstTimeRight=false;
                seekBarRight.setSeekBarType(SeekBarType.LEFT);
                long factor=(long) ((((float)(middleValue-(middleValue+5000))/middleValue))*MARGIN_LEFT*7);
                //System.out.println("-------haha haha left bar set runnable "+ (middleValue+4000+factor));
                seekBarRight.setSelectedMinValue(middleValue+5000+factor);
                canShowRight=true;
                rightStart=middleValue+5000-(-leftProgressRight) +factor+leftBarOffset;
                //rightStart=18000;
                rightBarValue=middleValue+5000-(-leftProgressRight) +factor+leftBarOffset;;
                //System.out.println("-------haha haha leftstart  kkk first "+ leftProgressRight);

                overlay.setVisibility(View.INVISIBLE);
                seekBarRight.setVisibility(View.VISIBLE);
                endSecond=time;
                isRightBarShowing=true;
            }
        }
    }

    private void setCurrentTime() {
        time=(double) (((leftProgress/2) + (middleValue/2))/1000.0);
        setTimeToTextView(mTvShootTipMiddle,time);
    }

    private void setPositionToMediaPlayer() {
        mediaPlayerTime=(int) ((leftProgress/2) + (middleValue/2) );

        if(mMediaPlayer!=null){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                mMediaPlayer.seekTo(mediaPlayerTime, MediaPlayer.SEEK_CLOSEST);
            }else{
                mMediaPlayer.seekTo(mediaPlayerTime);
            }
            //System.out.println("ken media time "+mediaPlayerTime);

        }
    }

    private void calculateLeftProgress(int scrollX) {
        scrollPos = (long) (averageMsPx * (MARGIN + scrollX));
        //Log.d(TAG, "-------scrollPos:>>>>>" + scrollPos);
        leftProgress = seekBar.getSelectedMinValue() + scrollPos;
        rightProgress = seekBar.getSelectedMaxValue() + scrollPos;
        //Log.d(TAG, "-------leftProgress:>>>>>" + leftProgress);
    }

    private void setButtonEnableDisable(long val, long factor, long valRight, long factorRight) {
        if(canShow){
            if(middleValue-(-val +factor+leftBarOffset)<0){
                if(endHereTv.isEnabled()){
                    endHereTv.setEnabled(false);
                    endHereTv.setImageResource(R.mipmap.end_here_cut_inactive);
                }

            }else{
                if(!endHereTv.isEnabled()){
                    endHereTv.setEnabled(true);
                    endHereTv.setImageResource(R.mipmap.end_here_cut);

                }
            }
        }

        if(canShowRight){
            if(middleValue-(-valRight+factorRight)>0){
                if(startHereTv.isEnabled()){
                    startHereTv.setEnabled(false);
                    startHereTv.setImageResource(R.mipmap.start_here_cut_inactive);
                }

            }else{
                if(!startHereTv.isEnabled()){
                    startHereTv.setEnabled(true);
                    startHereTv.setImageResource(R.mipmap.start_here_cut);
                }
            }
        }
    }


    @Override
    public void setImageToStartHereTv() {
        setImageToIvAndEnableDisable(startHereTv,true, R.mipmap.start_here_cut);

    }
}

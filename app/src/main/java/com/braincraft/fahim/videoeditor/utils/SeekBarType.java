package com.braincraft.fahim.videoeditor.utils;

public enum SeekBarType {
    LEFT,
    RIGHT,
    BOTH,
    EXCEPTIONAL,
    LEFT_CUT,
    RIGHT_CUT,
    NOTHING;
}

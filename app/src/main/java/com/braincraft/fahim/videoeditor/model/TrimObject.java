package com.braincraft.fahim.videoeditor.model;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.braincraft.fahim.videoeditor.R;
import com.braincraft.fahim.videoeditor.utils.ExtractFrameWorkThread;
import com.braincraft.fahim.videoeditor.utils.SeekBarType;
import com.braincraft.fahim.videoeditor.utils.UIUtils;
import com.braincraft.fahim.videoeditor.utils.VideoUtil;
import com.braincraft.fahim.videoeditor.view.RangeSeekBar;
import com.braincraft.fahim.videoeditor.view.VideoThumbSpacingItemDecoration;

import static com.braincraft.fahim.videoeditor.activity.MyTrimVideoActivity.MARGIN;
import static com.braincraft.fahim.videoeditor.activity.MyTrimVideoActivity.MARGIN_LEFT;
import static com.braincraft.fahim.videoeditor.activity.MyTrimVideoActivity.MAX_COUNT_RANGE;
import static com.braincraft.fahim.videoeditor.activity.MyTrimVideoActivity.MAX_CUT_DURATION;
import static com.braincraft.fahim.videoeditor.activity.MyTrimVideoActivity.MIN_CUT_DURATION;
import static com.braincraft.fahim.videoeditor.activity.MyTrimVideoActivity.setTimeToTextView;

public class TrimObject extends MainObject {


    public TrimObject(Context context, RecyclerView mRecyclerView, ImageView startHereTv, ImageView endHereTv, LinearLayout overlay, TextView startTimeTv, TextView endTimeTv, LinearLayout seekBarLayout, LinearLayout seekBarLayoutRight, TextView mTvShootTipMiddle) {
        super(context, mRecyclerView, startHereTv, endHereTv, overlay, startTimeTv, endTimeTv, seekBarLayout, seekBarLayoutRight, mTvShootTipMiddle);
    }

    @Override
    public void initEditVideo(long duration) {
        setImageToIvAndEnableDisable(endHereTv, false, R.mipmap.end_here_after_press_2);
        //for video edit
        long endPosition = duration;
        boolean isOver_10_s = identifyDuration(duration);
        initSeekBar(endPosition, isOver_10_s);
        initSeekBarRight(endPosition, isOver_10_s);
        setDefaultLeftAndRightProgress(endPosition, isOver_10_s);
        startHereTv.setOnClickListener(getStartHereTvClickListener());
        endHereTv.setOnClickListener(getEndHereTvClickListener());
        setDefaultVisibility();
        setThumbRatioToSeekBars();
    }

    private void setDefaultLeftAndRightProgress(long endPosition, boolean isOver_10_s) {
        leftProgress = 0;
        leftProgressRight = 0;
        if (isOver_10_s) {
            rightProgress = MAX_CUT_DURATION;
        } else {
            rightProgress = endPosition;
        }
    }

    private boolean identifyDuration(long duration) {
        boolean isOver_10_s = false;
        if (duration >= MAX_CUT_DURATION) {
            isOver_10_s = true;
        }
        return isOver_10_s;
    }

    private void setDefaultVisibility() {
        seekBar.setVisibility(View.INVISIBLE);
        seekBarRight.setVisibility(View.INVISIBLE);
        overlay.setVisibility(View.VISIBLE);
    }

    private void setThumbRatioToSeekBars() {
        BitmapDrawable d = (BitmapDrawable) context.getResources().getDrawable(R.mipmap.lower_image_bar_tool_left);
        double thumbRatio = d.getBitmap().getWidth() / (double) d.getBitmap().getHeight();
        seekBar.setThumbRatio(thumbRatio);
        seekBarRight.setThumbRatio(thumbRatio);
    }

    private View.OnClickListener getEndHereTvClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.isEnabled()) {
                    seekBarRight.setSelectedMinValue(middleValue);
                    canShowRight = true;
                    if (!isSeekingOnce) {
                        leftProgressRight = -5000;
                    }
                    rightStart = middleValue - (-leftProgressRight);
                    //leftStart=middleValue-(-leftProgress) + leftBarOffset;
                    overlay.setVisibility(View.INVISIBLE);
                    seekBarRight.setVisibility(View.VISIBLE);
                    endSecond = time;
                    setTimeToTextView(endTimeTv, endSecond);
                    setImageToIvAndEnableDisable(startHereTv, true, R.mipmap.start_here);
                }
            }
        };
    }

    private View.OnClickListener getStartHereTvClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (overlay.getVisibility() == View.VISIBLE) {
                    overlay.setVisibility(View.INVISIBLE);
                }
                seekBar.setSelectedMinValue(middleValue - leftBarOffset);
                canShow = true;
                if (!isSeekingOnce) {
                    leftProgress = -5000;
                }
                //System.out.println("trimk left progress "+leftProgress);
                leftStart = middleValue - (-leftProgress) - leftBarOffset;
                overlay.setVisibility(View.INVISIBLE);
                seekBar.setVisibility(View.VISIBLE);
                startSecond = time;
                setTimeToTextView(startTimeTv, startSecond);
                setImageToIvAndEnableDisable(endHereTv, true, R.mipmap.end_here_2);
            }
        };
    }

    private void initSeekBarRight(long endPosition, boolean isOver_10_s) {
        //init seekBar right
        if (isOver_10_s) {
            seekBarRight = new RangeSeekBar(context, 0L, MAX_CUT_DURATION, SeekBarType.RIGHT);
            seekBarRight.setSelectedMaxValue(MAX_CUT_DURATION);
        } else {
            seekBarRight = new RangeSeekBar(context, 0L, endPosition, SeekBarType.RIGHT);
            seekBarRight.setSelectedMaxValue(endPosition);
        }

        seekBarRight.setMin_cut_time(MIN_CUT_DURATION);
        seekBarRight.setNotifyWhileDragging(true);
        seekBarLayoutRight.addView(seekBarRight);
    }

    private void initSeekBar(long endPosition, boolean isOver_10_s) {
        if (isOver_10_s) {
            seekBar = new RangeSeekBar(context, 0L, MAX_CUT_DURATION, SeekBarType.LEFT);
            seekBar.setSelectedMaxValue(MAX_CUT_DURATION);
        } else {
            seekBar = new RangeSeekBar(context, 0L, endPosition, SeekBarType.LEFT);
            seekBar.setSelectedMaxValue(endPosition);
        }

        seekBar.setMin_cut_time(MIN_CUT_DURATION);
        seekBar.setNotifyWhileDragging(true);
        // seekBar.setOnRangeSeekBarChangeListener(mOnRangeSeekBarChangeListener);
        seekBarLayout.addView(seekBar);
    }

    @Override
    public void setLeftBarOffset(long leftBarOffset) {
        this.leftBarOffset = leftBarOffset;
    }

    @Override
    public void setScrollListener() {
        mRecyclerView.scrollToPosition(0);
        mRecyclerView.addOnScrollListener(mOnScrollListener);
    }

    @Override
    public void removeSeekBars() {
        seekBarLayout.removeView(seekBar);
        seekBarLayoutRight.removeView(seekBarRight);
    }

    @Override
    public void removeListener() {
        mRecyclerView.removeOnScrollListener(mOnScrollListener);
    }

    private RecyclerView.OnScrollListener mOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            //Log.d(TAG, "-------newState:>>>>>" + newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int scrollX = getScrollXDistance();
            boolean leftGoneThroughRight = false, rightGoneThroughRight = false;
            //System.out.println("scrollx "+scrollX);
            if (scrollX == -MARGIN) {
                scrollPos = 0;
            } else {
                if (!isSeekingOnce) {
                    isSeekingOnce = true;
                }
                calculateLeftProgress(scrollX);
                setPositionToMediaPlayer();
                setCurrentTime();

                long val = leftProgress - leftStart;
                long factor = (long) ((((float) (middleValue - (-val)) / middleValue)) * MARGIN_LEFT * 7);
                //System.out.println("left bar offset "+ leftBarOffset);
                leftGoneThroughRight = setLeftSeekBarVisibility(leftGoneThroughRight, val, factor);

                leftProgressRight = seekBarRight.getSelectedMinValue() + scrollPos;
                rightProgressRight = seekBarRight.getSelectedMaxValue() + scrollPos;
                long valRight = leftProgressRight - rightStart;
                long factorRight = (long) ((((float) (middleValue - (-valRight)) / middleValue)) * MARGIN_LEFT * 7);

                rightGoneThroughRight = setRightSeekBarVisibility(rightGoneThroughRight, valRight, factorRight);

                //System.out.println("trtr middle left "+ (middleValue-(-val))+" right "+(middleValue-(-valRight)));
                setButtonEnableDisable(val, factor, valRight, factorRight);

                if (leftGoneThroughRight && rightGoneThroughRight) {
                    overlay.setVisibility(View.VISIBLE);
                }
            }
            lastScrollX = scrollX;
        }
    };

    private void setButtonEnableDisable(long val, long factor, long valRight, long factorRight) {
        if (canShow) {
            if (middleValue - (-val + factor + leftBarOffset) < 0) {
                if (endHereTv.isEnabled()) {
                    setImageToIvAndEnableDisable(endHereTv, !endHereTv.isEnabled(), R.mipmap.end_here_after_press_2);
                }
            } else {
                if (!endHereTv.isEnabled()) {
                    setImageToIvAndEnableDisable(endHereTv, !endHereTv.isEnabled(), R.mipmap.end_here_2);
                }
            }
        }

        if (canShowRight) {
            if (middleValue - (-valRight + factorRight) > 0) {
                if (startHereTv.isEnabled()) {
                    setImageToIvAndEnableDisable(startHereTv, !startHereTv.isEnabled(), R.mipmap.start_here_after_press_2);
                }
            } else {
                if (!startHereTv.isEnabled()) {
                    setImageToIvAndEnableDisable(startHereTv, !startHereTv.isEnabled(), R.mipmap.start_here);
                }
            }
        }
    }

    private boolean setRightSeekBarVisibility(boolean rightGoneThroughRight, long valRight, long factorRight) {
        if ((valRight) > leftBarOffset) {
            seekBarRight.setVisibility(View.INVISIBLE);
            if (canShowRight) {
                overlay.setVisibility(View.VISIBLE);
            }
        } else if ((valRight) < -(middleValue * 2) - (leftBarOffset / 2)) {
            //overlay.setVisibility(View.VISIBLE);
            seekBarRight.setVisibility(View.INVISIBLE);
            rightGoneThroughRight = true;
        } else {
            if (canShowRight) {
                overlay.setVisibility(View.INVISIBLE);
                seekBarRight.setSelectedMinValue((-(valRight) + factorRight));
                seekBarRight.setVisibility(View.VISIBLE);
            }
        }
        return rightGoneThroughRight;
    }

    private boolean setLeftSeekBarVisibility(boolean leftGoneThroughRight, long val, long factor) {
        if ((val) > leftBarOffset) {
            seekBar.setVisibility(View.INVISIBLE);
            //System.out.println("trtrt sb invisible left shift");

        } else if ((val) < -((middleValue * 2) + (leftBarOffset / 2))) {
            overlay.setVisibility(View.VISIBLE);
            seekBar.setVisibility(View.INVISIBLE);
            leftGoneThroughRight = true;
            //System.out.println("trtrt  ol visible right shift");

        } else {
            if (canShow) {
                overlay.setVisibility(View.INVISIBLE);
                seekBar.setSelectedMinValue((-(val) + factor));
                seekBar.setVisibility(View.VISIBLE);
                //System.out.println("trtrt sb visible present");
            }

        }
        return leftGoneThroughRight;
    }

    private void calculateLeftProgress(int scrollX) {
        scrollPos = (long) (averageMsPx * (MARGIN + scrollX));
        //Log.d(TAG, "-------scrollPos:>>>>>" + scrollPos);
        leftProgress = seekBar.getSelectedMinValue() + scrollPos;
        rightProgress = seekBar.getSelectedMaxValue() + scrollPos;
        //Log.d(TAG, "-------leftProgress:>>>>>" + leftProgress);
    }

    private void setCurrentTime() {
        time = (double) (((leftProgress / 2) + (middleValue / 2)) / 1000.0);
        //System.out.println("kuku "+time+" kk "+ ((leftProgress/2) + (middleValue/2)));

        setTimeToTextView(mTvShootTipMiddle, time);
    }

    private void setPositionToMediaPlayer() {
        mediaPlayerTime = (int) ((leftProgress / 2) + (middleValue / 2));

        if (mMediaPlayer != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                mMediaPlayer.seekTo(mediaPlayerTime, MediaPlayer.SEEK_CLOSEST);
            } else {
                mMediaPlayer.seekTo(mediaPlayerTime);
            }
            System.out.println("ken media time " + mediaPlayerTime);
        }
    }


    @Override
    public void setImageToStartHereTv() {
        setImageToIvAndEnableDisable(startHereTv,true,R.mipmap.start_here);

    }
}

package com.braincraft.fahim.videoeditor.activity;

import android.Manifest;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Surface;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.braincraft.fahim.videoeditor.adapter.TrimVideoAdapter;
import com.braincraft.fahim.videoeditor.base.BaseActivity;
import com.braincraft.fahim.videoeditor.model.CutObject;
import com.braincraft.fahim.videoeditor.model.MainObject;
import com.braincraft.fahim.videoeditor.model.TrimObject;
import com.braincraft.fahim.videoeditor.model.VideoEditInfo;
import com.braincraft.fahim.videoeditor.utils.CustomToast;
import com.braincraft.fahim.videoeditor.utils.ExtractFrameWorkThread;
import com.braincraft.fahim.videoeditor.utils.ExtractVideoInfoUtil;
import com.braincraft.fahim.videoeditor.utils.SeekBarType;
import com.braincraft.fahim.videoeditor.utils.UIUtils;
import com.braincraft.fahim.videoeditor.utils.VideoUtil;
import com.braincraft.fahim.videoeditor.view.NormalProgressDialog;
import com.braincraft.fahim.videoeditor.view.RangeSeekBar;
import com.braincraft.fahim.videoeditor.view.VideoThumbSpacingItemDecoration;
import com.braincraft.fahim.videoeditor.R;
import com.braincraft.fahim.videoeffect.GlVideoView;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.io.File;
import java.lang.ref.WeakReference;
import java.math.BigDecimal;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class MyTrimVideoActivity extends BaseActivity {

    @BindView(R.id.glsurfaceview)
    GlVideoView mSurfaceView;

    @BindView(R.id.middle_time_tv)
    TextView mTvShootTipMiddle;

    @BindView(R.id.video_thumb_listview)
    RecyclerView mRecyclerView;
    @BindView(R.id.id_seekBarLayout)
    LinearLayout seekBarLayout;
    @BindView(R.id.id_seekBarLayout_right)
    LinearLayout seekBarLayoutRight;
    @BindView(R.id.relativeLayout2)
    ConstraintLayout mRlVideo;
    @BindView(R.id.overlay)
    LinearLayout overlay;
    @BindView(R.id.layout_trim_cut)
    View layout_trim_cut;
    @BindView(R.id.layout_video_full)
    View layout_video_full;
    @BindView(R.id.layout_edit_type)
    View layout_edit_type;
    @BindView(R.id.layout_start_end)
    View layout_start_end;
    @BindView(R.id.layout_video_frame)
    View layout_video_frame;
    @BindView(R.id.layout_show_time)
    View layout_show_time;
    @BindView(R.id.start_here)
    ImageView startHereTv;
    @BindView(R.id.end_here)
    ImageView endHereTv;
    @BindView(R.id.time_selector)
    ImageView timeSelector;
    @BindView(R.id.trim)
    TextView trimTv;
    @BindView(R.id.cut)
    TextView cutTv;
    @BindView(R.id.start_time_tv)
    TextView startTimeTv;
    @BindView(R.id.end_time_tv)
    TextView endTimeTv;

    @BindView(R.id.back)
    ImageView backButton;
    @BindView(R.id.done)
    ImageView doneButton;
    @BindView(R.id.imageView)
    ImageView playButton;
    @BindView(R.id.imageView2)
    ImageView cameraButton;
    @BindView(R.id.Simple)
    TextView simpleButton;
    @BindView(R.id.advanced)
    TextView advancedButton;
    @BindView(R.id.pullToRefresh)
    SwipeRefreshLayout swipeRefreshLayout;



    private static final String TAG = MyTrimVideoActivity.class.getSimpleName();
    public static final long MIN_CUT_DURATION = 3 * 1000L;
    public static final long MAX_CUT_DURATION = 10 * 1000L;
    public static final int MAX_COUNT_RANGE = 10;
    public static final int MARGIN = UIUtils.dp2Px(0);
    public static final int MARGIN_LEFT = UIUtils.dp2Px(16);
    public static final int MARGIN_THUMB = UIUtils.dp2Px(8);
    private ExtractVideoInfoUtil mExtractVideoInfoUtil;
    private int mMaxWidth;
    private long duration;
    private TrimVideoAdapter videoEditAdapter;
    private float averageMsPx;
    private String OutPutFileDirPath;
    private ExtractFrameWorkThread mExtractFrameWorkThread;
    private String mVideoPath;
    private SurfaceTexture mSurfaceTexture;
    private MediaPlayer mMediaPlayer;
    private long middleValue,leftBarOffset;
    private double thumbOffsetRatio=200/(double)MARGIN_THUMB;
    boolean isTrim=true;

    private RxPermissions mRxPermissions;
    private Boolean granted;
    CustomToast customToast;

    MainObject mainObject;



    @Override
    protected int getLayoutId() {
        return R.layout.layout_combined_test;
    }

    @Override
    protected void init() {
        mVideoPath = getIntent().getStringExtra("videoPath");

        mExtractVideoInfoUtil = new ExtractVideoInfoUtil(mVideoPath);
        mMaxWidth = UIUtils.getScreenWidth() - MARGIN * 2;

        Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(ObservableEmitter<String> e) {
                e.onNext(mExtractVideoInfoUtil.getVideoLength());
                e.onComplete();
            }
        })
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<String>() {
                @Override
                public void onSubscribe(Disposable d) {
                   subscribe(d);
                }

                @Override
                public void onNext(String s) {
                    setDuration();
                    setMiddleValue();
                    initFrameLoading();
                   //+
                    // initMainObject();
                   // initEditVideo();
                }

                @Override
                public void onError(Throwable e) {

                }

                @Override
                public void onComplete() {

                }
            });

    }

    private void initMainObject() {
        System.out.println("trim from obers");
        if(isTrim){
            mainObject= new TrimObject(MyTrimVideoActivity.this,mRecyclerView,startHereTv,endHereTv,overlay,startTimeTv,endTimeTv,seekBarLayout,seekBarLayoutRight,mTvShootTipMiddle);
        }else{
            mainObject= new CutObject(MyTrimVideoActivity.this,mRecyclerView,startHereTv,endHereTv,overlay,startTimeTv,endTimeTv,seekBarLayout,seekBarLayoutRight,mTvShootTipMiddle);

        }
        mainObject.setDuration(duration);
        mainObject.setMiddleValue(middleValue);
        mainObject.setAverageMsPx(averageMsPx);
        mainObject.setLeftBarOffset(leftBarOffset);
        mainObject.setMediaPlayer(mMediaPlayer);

        mainObject.initEditVideo(duration);
        mainObject.setScrollListener();

    }


    private void initFrameLoading() {
        //for video edit
        long startPosition = 0;
        long endPosition = duration;
        int thumbnailsCount;
        int rangeWidth;
        if (endPosition <= MAX_CUT_DURATION) {
            thumbnailsCount = (int)duration/1000;
            rangeWidth = mMaxWidth;

        } else {
            thumbnailsCount = (int) (endPosition * 1.0f / (MAX_CUT_DURATION * 1.0f)
                    * MAX_COUNT_RANGE);
            rangeWidth = mMaxWidth / MAX_COUNT_RANGE * thumbnailsCount;
        }

        averageMsPx = duration * 1.0f / rangeWidth * 1.0f;
        OutPutFileDirPath = VideoUtil.getSaveEditThumbnailDir(this);
        int extractW = mMaxWidth / MAX_COUNT_RANGE;
        int extractH = UIUtils.dp2Px(62);


            mRecyclerView
                    .addItemDecoration(new VideoThumbSpacingItemDecoration(MARGIN, thumbnailsCount));
            mExtractFrameWorkThread = new ExtractFrameWorkThread(extractW, extractH, mUIHandler,
                    mVideoPath,
                    OutPutFileDirPath, startPosition, endPosition, thumbnailsCount);
            mExtractFrameWorkThread.start();


    }



    private void setMiddleValue() {
        if(duration<MAX_CUT_DURATION){
            middleValue=duration/2;
        }else{
            middleValue=MAX_CUT_DURATION/2;
        }
    }

    private void setDuration() {
        duration = Long.valueOf(mExtractVideoInfoUtil.getVideoLength());
        float tempDuration = duration / 1000f;
        duration = new BigDecimal(tempDuration).setScale(0, BigDecimal.ROUND_HALF_UP).intValue() * 1000;
    }

    @Override
    protected void initView() {
        mRxPermissions= new RxPermissions(this);
        takePermission();

        mSurfaceView.init(surfaceTexture -> {
            mSurfaceTexture = surfaceTexture;
            initMediaPlayer(surfaceTexture);
            System.out.println("trim from media");
            initMainObject();

        });

        mRecyclerView
            .setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                videoEditAdapter = new TrimVideoAdapter(MyTrimVideoActivity.this, mMaxWidth / 5,mRecyclerView.getHeight());
                mRecyclerView.setAdapter(videoEditAdapter);

                System.out.println("trim from init");
               // mRecyclerView.addOnScrollListener(mOnScrollListener);

                BitmapDrawable d= (BitmapDrawable) MyTrimVideoActivity.this.getResources().getDrawable(R.mipmap.lower_image_bar_tool_left);
                double thumbRatio= d.getBitmap().getWidth()/(double)d.getBitmap().getHeight();
                int w= (int) (mRecyclerView.getHeight()*thumbRatio);
                leftBarOffset=(long) (w*thumbOffsetRatio);



            }
        },50);

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(granted){
                    double startSecond=mainObject.getStartSecond();
                    double endSecond=mainObject.getEndSecond();
                    if(endSecond -startSecond>0){
                        trimmerVideo(isTrim,startSecond,endSecond);
                    }else{
                        customToast.showToast("Attention!","Video Length must be greater than 0 second",Toast.LENGTH_LONG);
                    }
                }else{
                    takePermission();
                }

            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        customToast= new CustomToast(this);




        swipeRefreshLayout.setRefreshing(false);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    private void refreshData() {

     startHomeActivity();

    }
    private void startHomeActivity() {
        Intent homeIntent = new Intent(this, MyTrimVideoActivity.class);
        homeIntent.putExtra("videoPath",mVideoPath);
        startActivity(homeIntent);
        finishAffinity();
    }

    @OnClick({R.id.trim, R.id.cut,R.id.imageView,R.id.imageView2,R.id.Simple,R.id.advanced})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.trim:
                if(!isTrim){
                    isTrim=true;
                    doOnClickAction();
                }

                break;
            case R.id.cut:
                if(isTrim){
                    isTrim=false;
                    doOnClickAction();
                }
                break;

            case R.id.imageView:
                customToast.showToast("Hello!", "Clicked on play",Toast.LENGTH_SHORT);
                break;

            case R.id.imageView2:
                customToast.showToast("Hello!", "Clicked on Camera",Toast.LENGTH_SHORT);
                break;

            case R.id.Simple:
                customToast.showToast("Hello!", "Clicked on Simple",Toast.LENGTH_SHORT);
                break;

            case R.id.advanced:
                customToast.showToast("Hello!", "Clicked on Advanced",Toast.LENGTH_SHORT);
                break;

        }
    }

    private void doOnClickAction() {
        mainObject.resetValues();
        mainObject.removeSeekBars();
        mainObject.removeListener();
        initMainObject();
        mainObject.setImageToStartHereTv();
        mainObject.setTimesToTextView();
        setButtonsColor();
    }

    private void setButtonsColor() {
        if(isTrim){
            trimTv.setTextColor(getResources().getColor(R.color.white));
            trimTv.setBackground(getResources().getDrawable(R.drawable.background_fill_pressed,null));
            cutTv.setTextColor(getResources().getColor(R.color.magenta));
            cutTv.setBackground(getResources().getDrawable(R.drawable.background_fill_right,null));
        }else{
            cutTv.setTextColor(getResources().getColor(R.color.white));
            cutTv.setBackground(getResources().getDrawable(R.drawable.background_fill_right_pressed,null));
            trimTv.setTextColor(getResources().getColor(R.color.magenta));
            trimTv.setBackground(getResources().getDrawable(R.drawable.background_fill,null));
        }

    }


    private void initMediaPlayer(SurfaceTexture surfaceTexture) {
        mMediaPlayer = new MediaPlayer();
        try {
            mMediaPlayer.setDataSource(mVideoPath);
            Surface surface = new Surface(surfaceTexture);
            mMediaPlayer.setSurface(surface);
            surface.release();
            mMediaPlayer.setLooping(true);
            mMediaPlayer.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    ViewGroup.LayoutParams lp = mSurfaceView.getLayoutParams();
                    int videoWidth = mp.getVideoWidth();
                    int videoHeight = mp.getVideoHeight();
                    float videoProportion = (float) videoWidth / (float) videoHeight;
                    int screenWidth = mRlVideo.getWidth();
                    int screenHeight = mRlVideo.getHeight();
                    float screenProportion = (float) screenWidth / (float) screenHeight;
                    if (videoProportion > screenProportion) {
                        lp.width = screenWidth;
                        lp.height = (int) ((float) screenWidth / videoProportion);
                    } else {
                        lp.width = (int) (videoProportion * (float) screenHeight);
                        lp.height = screenHeight;
                    }
                    mSurfaceView.setLayoutParams(lp);


                    //Log.e("videoView", "videoWidth:" + videoWidth + ", videoHeight:" + videoHeight);
                }
            });
            mMediaPlayer.prepare();
            //mainObject.setMediaPlayer(mMediaPlayer);
            //videoStart();
        } catch (Exception e) {
            //System.out.println("----videoPause crash");
            e.printStackTrace();
        }
    }


    private void trimmerVideo(boolean isTrim, double startSecond, double endSecond) {
        NormalProgressDialog
            .showLoading(this, getResources().getString(R.string.in_process), false);

        String prefix="Trim";
        if(!isTrim){
            prefix="Cut";
        }
        String finalPrefix = prefix;
        if(isTrim){
            VideoUtil
                    .cutVideo(mVideoPath, VideoUtil.getTrimmedVideoPath(this, "/Fahim_BrainCraft",
                            prefix+"Video_"), startSecond,
                            endSecond)
                    .subscribe(getVideoTrimCutObserver(finalPrefix));
        }else{
            VideoUtil
                    .cutVideo(mVideoPath, VideoUtil.getTrimmedVideoPath(this, "/Fahim_BrainCraft",
                            prefix+"Video_"),0, startSecond,
                            endSecond,duration/1000)
                    .subscribe(getVideoTrimCutObserver(finalPrefix));
        }

    }

    private Observer<String> getVideoTrimCutObserver(String finalPrefix) {
        return new Observer<String>() {
            @Override
            public void onSubscribe(Disposable d) {
                subscribe(d);
            }

            @Override
            public void onNext(String outputPath) {

                try {
                    NormalProgressDialog.stopLoading();
                    customToast.showToast("Video Saved in:",outputPath,Toast.LENGTH_LONG);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                //Log.e(TAG, "cutVideo---onError:" + e.toString());
                NormalProgressDialog.stopLoading();
                customToast.showToast("Sorry!","Something bad happened :(",Toast.LENGTH_SHORT);
            }

            @Override
            public void onComplete() {
            }
        };
    }

    public static void setTimeToTextView(TextView textView, double time) {
        String prefix = "00:";
        if (time < 10) {
            prefix = "00:0";
        }
        textView.setText(prefix + (int) time);
    }

    private final MainHandler mUIHandler = new MainHandler(this);

    private static class MainHandler extends Handler {

        private final WeakReference<MyTrimVideoActivity> mActivity;

        MainHandler(MyTrimVideoActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            MyTrimVideoActivity activity = mActivity.get();
            if (activity != null) {
                if (msg.what == ExtractFrameWorkThread.MSG_SAVE_SUCCESS) {
                    if (activity.videoEditAdapter != null) {
                        VideoEditInfo info = (VideoEditInfo) msg.obj;
                        activity.videoEditAdapter.addItemVideoInfo(info);
                    }
                }
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    protected void onDestroy() {
        NormalProgressDialog.stopLoading();

        if (mMediaPlayer != null) {
            mMediaPlayer.release();
        }

        if (mExtractVideoInfoUtil != null) {
            mExtractVideoInfoUtil.release();
        }
        if (mExtractFrameWorkThread != null) {
            mExtractFrameWorkThread.stopExtract();
        }
        mainObject.removeListener();
        mUIHandler.removeCallbacksAndMessages(null);
        if (!TextUtils.isEmpty(OutPutFileDirPath)) {
            VideoUtil.deleteFile(new File(OutPutFileDirPath));
        }
        String trimmedDirPath = VideoUtil.getTrimmedVideoDir(this, "small_video/trimmedVideo");
        if (!TextUtils.isEmpty(trimmedDirPath)) {
            VideoUtil.deleteFile(new File(trimmedDirPath));
        }
        super.onDestroy();
    }

    public void takePermission() {
        mRxPermissions
                .request(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        subscribe(d);
                    }

                    @Override
                    public void onNext(Boolean granted) {
                        MyTrimVideoActivity.this.granted=granted;
                        if (granted) {


                        } else {
                            customToast.showToast("Attention!", "Video will not be saved if you do not grant permission", Toast.LENGTH_LONG);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
